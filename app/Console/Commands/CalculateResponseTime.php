<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Proexe\BookingApp\Bookings\Models\BookingModel;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;

class CalculateResponseTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookingApp:calculateResponseTime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates response time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $bookings = BookingModel::with('office')->get()->toArray();
        $responseTimeCalculator = new ResponseTimeCalculator();
        // var_dump($bookings);

        foreach( $bookings as $booking ) {
            $responseTime = $responseTimeCalculator->calculate($booking['created_at'], $booking['updated_at'], $booking['office']['office_hours']);
            $this->line("created_at: " . $booking['created_at']);
            $this->line("updated_at: " . $booking['updated_at']);

            $this->line("Response time will be: $responseTime minutes");
            $this->line("====================");
            $this->line("");
        }
    }
}
