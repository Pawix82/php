<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		$from_x = floatval($from[0]);
		$from_y = floatval($from[1]);

		$to_x = floatval($to[0]);
		$to_y = floatval($to[1]);
		
		$distance = sqrt(pow($to_x - $from_x, 2) + pow(cos(($from_x * pi()) / 180) * ($to_y - $from_y), 2)) * 111.3214;

		if($unit == 'm') {
			return $distance * 1000;
		}

		return round($distance, 2);
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
		$offices_arr = [];

		foreach($offices as $office) {
			$offices_arr[$office['name']] = $this->calculate($from, [ $office['lat'], $office['lng'] ], 'km'); 
		}
		
		asort($offices_arr);

		$office = array_keys($offices_arr);
		return $office[0];

		// PURE SQL
		//
		// SELECT name, lat, lng, SQRT(
		// 	POW(110.574 * (lat - 14.12232322), 2) +
		// 	POW(110.574 * (8.12232322 - lng) * COS(lat / 57.3), 2)) AS distance
		// FROM offices ORDER BY distance LIMIT 1;
	}

}