<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use DateTime;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {

	public function calculate( $bookingDateTime, $responseDateTime, $officeHours ) {
		$modifiedBookingTime = new DateTime($bookingDateTime);
		$bookingDateTime = new DateTime($bookingDateTime);
		$responseDateTime = new DateTime($responseDateTime);
		

		$bookingDay = (int) $bookingDateTime->format('d');
		$responseDay = (int) $responseDateTime->format('d');

		$responseTime = 0;
		for($i = $bookingDay; $i <= $responseDay; $i++) {
			$dayOfWeek = (int) $modifiedBookingTime->format('w');
			if($officeHours[$dayOfWeek]['isClosed']) {
				$modifiedBookingTime->modify("+1 day");
				continue;
			}

			
			$from = $officeHours[$dayOfWeek]["from"];
			$to = $officeHours[$dayOfWeek]["to"];


			if($modifiedBookingTime == $bookingDateTime) {
				if(strtotime($to) < strtotime($bookingDateTime->format("H:i:s"))) {
					$modifiedBookingTime->modify("+1 day");
					continue;
				}

				if($bookingDateTime->format('Y-m-d') == $responseDateTime->format('Y-m-d')) {
					$from_time = $bookingDateTime->format('Y-m-d H:i:s');
					$to_time = $responseDateTime->format('Y-m-d H:i:s');
					$responseTime += (strtotime($to_time) - strtotime($from_time)) / 60;
					continue;
				}
				
				$to_time = $bookingDateTime->format('Y-m-d ').$to;
				$responseTime += (strtotime($to_time) - strtotime($bookingDateTime->format('Y-m-d H:i:s'))) / 60;				
				
				$modifiedBookingTime->modify("+1 day");
				continue;
			}

			if($modifiedBookingTime->format('Y-m-d') == $responseDateTime->format('Y-m-d')) {
				$from_time = $responseDateTime->format('Y-m-d ').$from;
				$responseTime += (strtotime($responseDateTime->format('Y-m-d H:i:s')) - strtotime($from_time)) / 60;	
				$modifiedBookingTime->modify("+1 day");
				continue;
			} else {
				$from_time = $modifiedBookingTime->format('Y-m-d ').$from;
				$to_time = $modifiedBookingTime->format('Y-m-d ').$to;
				$responseTime += (strtotime($to_time) - strtotime($from_time)) / 60;
				$modifiedBookingTime->modify("+1 day");
				continue;
			}
			
			
		}

		return $responseTime;
	}
}