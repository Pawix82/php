<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;

class BookingResponseTimeTest extends TestCase
{
    /** @test */
    public function testTask4()
    {
        $responseTimeCalculator = new ResponseTimeCalculator();

        $officeHours = [
            0 => [
                'isClosed' => true
            ],
            1 => [
                'from' => '8:00',
                'to' => '18:00',
                'isClosed' => false
            ],
            2 => [
                'from' => '7:00',
                'to' => '16:00',
                'isClosed' => false
            ],
            3 => [
                'from' => '10:00',
                'to' => '18:00',
                'isClosed' => false
            ],
            4 => [
                'from' => '9:00',
                'to' => '17:00',
                'isClosed' => false
            ],
            5 => [
                'from' => '8:00',
                'to' => '16:00',
                'isClosed' => false
            ],
            6 => [
                'from' => '10:00',
                'to' => '14:00',
                'isClosed' => false
            ]
        ];

        $this->assertEquals(269, $responseTimeCalculator->calculate("2021-04-19 16:31", "2021-04-20 10:00", $officeHours));
        $this->assertEquals(301, $responseTimeCalculator->calculate("2021-04-17 10:01", "2021-04-19 9:02", $officeHours));
        $this->assertEquals(9, $responseTimeCalculator->calculate("2021-04-23 9:01", "2021-04-23 9:10", $officeHours));
        $this->assertEquals(63, $responseTimeCalculator->calculate("2021-04-18 13:00", "2021-04-19 9:03", $officeHours));
        $this->assertEquals(120, $responseTimeCalculator->calculate("2021-04-19 21:00", "2021-04-20 9:00", $officeHours));
        $this->assertEquals(960, $responseTimeCalculator->calculate("2021-04-19 12:00", "2021-04-21 11:00", $officeHours));
    }
}
